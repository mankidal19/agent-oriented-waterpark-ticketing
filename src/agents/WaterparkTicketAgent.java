/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package agents;

import gui.FakeVariablesControllerUI;
import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;
import jade.lang.acl.ACLMessage;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import models.Ticket;
import models.TicketAvailable;
import models.TicketPurchase;
import utils.MyUtils;

/**
 *
 * @author NURUL AIMAN
 */
public class WaterparkTicketAgent extends Agent {

    private ArrayList<TicketAvailable> availableTicketList = new ArrayList<>();
    private boolean status = false;
    private int adultQtyPurchase = 0, childQtyPurchase = 0, seniorQtyPurchase = 0;
    private double adultTixPrice, childTixPrice, seniorTixPrice;
    private int adultQtyAvail = -1, childQtyAvail = -1, seniorQtyAvail = -1;
    private int step = 0;
    private TicketPurchase purchaseRequest = new TicketPurchase();
    
    @Override
    protected void setup() {

        MyUtils.setupControllerGUI(this);

        addBehaviour(new CyclicBehaviour() {
            @Override
            public void action() {

                ACLMessage msg = blockingReceive();
                

                //int step = 0;
                if (msg != null) {
                    String msgContent = msg.getContent();
                    System.out.println("\n[WaterparkTicketAgent] Message Received");
                    System.out.println("[WaterparkTicketAgent] Sender Agent   : " + msg.getSender());
                    System.out.println("[WaterparkTicketAgent] Message content [Base64 string]: " + msgContent);
                    System.out.println("[WaterparkTicketAgent] Step: " + step);

                    //TODO: checks availability
                    if (step == 0) {

                        try {
                            purchaseRequest = (TicketPurchase) MyUtils.deserializeObjectFromString(msgContent);
                        } catch (Exception ex) {
                            System.out.println("\n[WaterparkTicketAgent] StrToObj conversion error: " + ex.getMessage());
                        }

                        status = checkAvailability(purchaseRequest);

                        // if available
                        if (status) {
                            step++;

                            //calculate total price
                            double totalPrice = adultTixPrice * adultQtyPurchase + childTixPrice * childQtyPurchase + seniorTixPrice * seniorQtyPurchase;
                            purchaseRequest.setTotalAmount(totalPrice);
                            System.out.println("\n[WaterparkTicketAgent] Set purchaseRequest total amount: " + totalPrice);

                            //dummY txn code
                            purchaseRequest.setTxnCode("ADG345");

                            String strObj = "";
                            try {
                                strObj = MyUtils.serializeObjectToString(purchaseRequest);
                            } catch (Exception ex) {
                                System.out.println("\n[WaterparkTicketAgent] ObjToStr conversion error: " + ex.getMessage());
                            }

                            ACLMessage reply = new ACLMessage(ACLMessage.INFORM);

                            reply.addReceiver(msg.getSender()); //get from envelope                      

                            reply.setContent(strObj);
                            send(reply);

                        } //if not available, refuse
                        else {
                            ACLMessage reply = new ACLMessage(ACLMessage.REFUSE);

                            reply.addReceiver(msg.getSender()); //get from envelope                      

                            reply.setPerformative(ACLMessage.REFUSE);

                            reply.setContent("Tickets unavailable on desired date or/and amount");

                            send(reply);

                            MyUtils.resetStepAllAgent();
                        }

                    } //TODO: confirm payment
                    else if (step == 1) {
                        step++;
                        switch (msg.getPerformative()) {
                            case ACLMessage.FAILURE:
                                System.out.println("[WaterparkTicketAgent] payment failed");
                                MyUtils.resetStepAllAgent();
                                break;
                            case ACLMessage.INFORM:
                                System.out.println("[WaterparkTicketAgent] payment success");
                                adultQtyAvail -= adultQtyPurchase;
                                childQtyAvail -= childQtyPurchase;
                                seniorQtyAvail -= seniorQtyPurchase;
                                
                                MyUtils.updateTicketAvailableUI(purchaseRequest.getDate(),adultQtyAvail,childQtyAvail,seniorQtyAvail);
                                break;

                        }
                    }
                }

                //reset step value
                if (step > 1) {
                    resetStep();
                }

                System.out.println("[WaterparkTicketAgent] CyclicBehaviour Block");
                block();

            }

            private boolean checkAvailability(TicketPurchase purchaseRequest) {

                for (TicketAvailable ticket : availableTicketList) {

                    if (ticket.getDate().equals(purchaseRequest.getDate())) {

                        if (ticket.getTicket().getType().equals(Ticket.Type.ADULT)) {
                            adultQtyPurchase = (Integer) purchaseRequest.getPurchase().get(ticket.getTicket().getType());
                            adultQtyAvail = ticket.getQuantity();
                            System.out.println("[WaterparkTicketAgent] ADULT tix available: " + adultQtyAvail);
                            System.out.println("[WaterparkTicketAgent] ADULT tix intended to purchase: " + adultQtyPurchase);
                        } else if (ticket.getTicket().getType().equals(Ticket.Type.CHILD)) {
                            childQtyPurchase = (Integer) purchaseRequest.getPurchase().get(ticket.getTicket().getType());
                            childQtyAvail = ticket.getQuantity();
                            System.out.println("[WaterparkTicketAgent] CHILD tix available: " + childQtyAvail);
                            System.out.println("[WaterparkTicketAgent] CHILD tix intended to purchase: " + childQtyPurchase);
                        } else if (ticket.getTicket().getType().equals(Ticket.Type.SENIOR)) {
                            seniorQtyPurchase = (Integer) purchaseRequest.getPurchase().get(ticket.getTicket().getType());
                            seniorQtyAvail = ticket.getQuantity();
                            System.out.println("[WaterparkTicketAgent] SENIOR tix available: " + seniorQtyAvail);
                            System.out.println("[WaterparkTicketAgent] ADULT tix intended to purchase: " + seniorQtyPurchase);
                        }

                    }
                }

                if (adultQtyAvail >= adultQtyPurchase && childQtyAvail >= childQtyPurchase && seniorQtyAvail >= seniorQtyPurchase) {
                    return true;
                } else {
                    return false;
                }

            }

        });
    }

    public void resetStep() {
        step = 0;
    }

    public void updateTicketAvailability(ArrayList<TicketAvailable> ticketList) {
        availableTicketList.clear();

        for (TicketAvailable ticket : ticketList) {
            availableTicketList.add(ticket);
            System.out.println("[WaterparkTicketAgent] Update ticket available: " + ticket.getDate() + ", " + ticket.getTicket().getType() + ", " + ticket.getQuantity());

        }

        adultTixPrice = ticketList.get(0).getTicket().getPrice();
        childTixPrice = ticketList.get(3).getTicket().getPrice();
        seniorTixPrice = ticketList.get(6).getTicket().getPrice();

    }

}
