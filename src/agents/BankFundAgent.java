/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package agents;

import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;
import jade.lang.acl.ACLMessage;
import models.Ticket;
import models.TicketAvailable;
import models.TicketPurchase;
import utils.MyUtils;

/**
 *
 * @author NURUL AIMAN
 */
public class BankFundAgent extends Agent {

    private boolean status = false;
    private double fundAvailable = 0.00;
    private int step = 0;
    private TicketPurchase purchaseRequest = new TicketPurchase();

    @Override
    protected void setup() {

        MyUtils.setupControllerGUI(this);

        addBehaviour(new CyclicBehaviour() {
            @Override
            public void action() {

                ACLMessage msg = blockingReceive();

                //step = 0;
                if (msg != null) {
                    String msgContent = msg.getContent();
                    System.out.println("\n[BankFundAgent] Message Received");
                    System.out.println("[BankFundAgent] Sender Agent   : " + msg.getSender());
                    System.out.println("[BankFundAgent] Message content [Base64 string]: " + msgContent);
                    System.out.println("[BankFundAgent] Step: " + step);

                    //TODO: checks availability
                    if (step == 0) {

                        try {
                            purchaseRequest = (TicketPurchase) MyUtils.deserializeObjectFromString(msgContent);
                        } catch (Exception ex) {
                            System.out.println("\n[BankFundAgent] StrToObj conversion error: " + ex.getMessage());
                        }

                        status = checkFundStatus(purchaseRequest);

                        // if fund enough
                        if (status) {
                            step++;

                            //proceed payment to BankPaymentAgent
                            //Send messages to "bpa - BankPaymentAgent"        
                            ACLMessage reqMsg = new ACLMessage(ACLMessage.REQUEST);
                            reqMsg.setContent("Request proceed payment.");
                            reqMsg.addReceiver(new AID("bpa", AID.ISLOCALNAME));
                            send(reqMsg);

                            System.out.println("\n[BankFundAgent] Sending Message!");
                            System.out.println("[BankFundAgent] Receiver Agent                 : bpa");

                        } //if not enough, refuse
                        else {
                            ACLMessage reply = new ACLMessage(ACLMessage.REFUSE);

                            reply.addReceiver(msg.getSender()); //get from envelope                      

                            reply.setPerformative(ACLMessage.REFUSE);

                            reply.setContent("Not enough funds available");
                            System.out.println("\n[BankFundAgent] Not enough funds available!");
                            send(reply);
                            //MyUtils.resetStepAllAgent();
                        }

                    } else if (step == 1) {//step 1, receive reply from bpa

                        step++;
                        switch (msg.getPerformative()) {

                            case ACLMessage.FAILURE:
                                //insufficient fund
                                ACLMessage reply = new ACLMessage(ACLMessage.FAILURE);

                                reply.addReceiver(new AID("pa", AID.ISLOCALNAME));

                                reply.setPerformative(ACLMessage.FAILURE);

                                reply.setContent("Payment failed due to server offline");
                                System.out.println("\n[BankFundAgent] Payment failed due to server offline!");

                                send(reply);
                                //MyUtils.resetStepAllAgent();
                                break;
                            case ACLMessage.INFORM:
                                //tickets available

                                System.out.println("[BankFundAgent] OLD Fund available: " + fundAvailable);
                                System.out.println("[BankFundAgent] Purchase amount: " + purchaseRequest.getTotalAmount());

                                fundAvailable -= purchaseRequest.getTotalAmount();

                                System.out.println("[BankFundAgent] NEW Fund available: " + fundAvailable);

                                MyUtils.updateFundBalanceUI(fundAvailable);

                                ACLMessage reply2 = new ACLMessage(ACLMessage.INFORM);

                                reply2.addReceiver(new AID("pa", AID.ISLOCALNAME));

                                reply2.setContent("Fund deducted");
                                System.out.println("\n[BankFundAgent] Fund deducted!");

                                send(reply2);
                                break;
                        }
                    }
                }

                //reset step value
                if (step > 1) {
                    resetStep();
                }

                System.out.println("[BankFundAgent] CyclicBehaviour Block");
                block();

            }

            private boolean checkFundStatus(TicketPurchase purchaseRequest) throws NullPointerException {

                if (purchaseRequest != null) {
                    System.out.println("[BankFundAgent] Fund available: " + fundAvailable);
                    System.out.println("[BankFundAgent] Purchase amount: " + purchaseRequest.getTotalAmount());

                    if (fundAvailable >= purchaseRequest.getTotalAmount()) {
                        return true;
                    } else {
                        return false;
                    }
                } else {
                    return false;
                }

            }

        });
    }

    public void updateFund(double fund) {
        fundAvailable = fund;
    }

    public void resetStep() {
        step = 0;
    }
}
