/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package agents;

import gui.PersonalAgentUI;
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.Behaviour;
import jade.core.behaviours.CyclicBehaviour;
import jade.core.behaviours.OneShotBehaviour;
import jade.core.behaviours.SimpleBehaviour;
import jade.lang.acl.ACLMessage;
import java.awt.Color;
import java.text.ParseException;
import java.util.logging.Level;
import java.util.logging.Logger;
import models.TicketPurchase;
import utils.MyUtils;

/**
 *
 * @author NURUL AIMAN
 */
public class PersonalAgent extends Agent {

    private PersonalAgentUI myUI;
    private TicketPurchase myPurchase;
    private int step = 0;

    @Override
    protected void setup() {

        try {
            myUI = new PersonalAgentUI(this);
        } catch (ParseException ex) {
            Logger.getLogger(PersonalAgent.class.getName()).log(Level.SEVERE, null, ex);
        }

        myUI.showGui();

        //for receiving availability result 
        addBehaviour(new CyclicBehaviour(this) {
            boolean done = false;

            @Override
            public void action() {
                ACLMessage msg = blockingReceive();
                TicketPurchase purchaseRequest = new TicketPurchase();

                if (msg != null) {

                    if (step == 1) {
                        //step++;
                        String msgContent = msg.getContent();
                        System.out.println("\n[PersonalAgent] Message Received");
                        System.out.println("[PersonalAgent] Sender Agent   : " + msg.getSender());
                        System.out.println("[PersonalAgent] Message content [Base64 string]: " + msgContent);
                        
                        switch (msg.getPerformative()) {

                            case ACLMessage.REFUSE:
                            //case ACLMessage.FAILURE:    
                                done = true;
                                myUI.updateGuiStatusText("NOT AVAILABLE", Color.red);
                                resetStep();
                                //MyUtils.resetStepAllAgent();
                                break;
                            case ACLMessage.INFORM:
                                //tickets available
                                System.out.println("\n[PersonalAgent] tickets available");

                                try {
                                    purchaseRequest = (TicketPurchase) MyUtils.deserializeObjectFromString(msgContent);
                                } catch (Exception ex) {
                                    System.out.println("\n[PersonalAgent] StrToObj conversion error: " + ex.getMessage());
                                }

                                myUI.updateGuiAvailable(purchaseRequest.getTxnCode(), purchaseRequest.getTotalAmount());

                                checkFund(purchaseRequest);
                                break;
                        }
                    } else if (step == 2) {
                        step++;
                        switch (msg.getPerformative()) {
                            case ACLMessage.REFUSE://due to insufficient fund
                            case ACLMessage.FAILURE: //due to offline server
                                done = true;
                                String reason;
                                if(msg.getPerformative() == ACLMessage.REFUSE){
                                    reason = "INSUFFICIENT FUND";
                                }
                                else{
                                    reason = "BANK SERVER OFFLINE";
                                }
                                
                                myUI.updateGuiStatusText(reason, Color.red);
                                System.out.println("\n[PersonalAgent] payment failed: " + reason);

                                //inform payment done to ta
                                ACLMessage paymentFailedMsg = new ACLMessage(ACLMessage.FAILURE);

                                paymentFailedMsg.setContent("Payment failed " + reason);
                                paymentFailedMsg.addReceiver(new AID("ta", AID.ISLOCALNAME));
                                send(paymentFailedMsg);

                                System.out.println("\n[PersonalAgent] Sending Message!");
                                System.out.println("[PersonalAgent] Receiver Agent                 : ta");
                                System.out.println("[PersonalAgent] Message content [Base64 string]: " + paymentFailedMsg.getContent());
                                resetStep();
                                //MyUtils.resetStepAllAgent();
                                break;

                            case ACLMessage.INFORM:
                                myUI.updateGuiStatusText("PAYMENT SUCCESS", Color.green);
                                //myUI.updateGuiAvailable(purchaseRequest.getTxnCode(), purchaseRequest.getTotalAmount());
                                
                                
                                System.out.println("\n[PersonalAgent] payment success");

                                //inform payment done to ta
                                ACLMessage paymentDoneMsg = new ACLMessage(ACLMessage.INFORM);

                                paymentDoneMsg.setContent("Payment done");
                                paymentDoneMsg.addReceiver(new AID("ta", AID.ISLOCALNAME));
                                send(paymentDoneMsg);

                                System.out.println("\n[PersonalAgent] Sending Message!");
                                System.out.println("[PersonalAgent] Receiver Agent                 : ta");
                                System.out.println("[PersonalAgent] Message content [Base64 string]: " + paymentDoneMsg.getContent());
                                break;
                        }

                    }
                }

            }

//            @Override
//            public boolean done() {
//                if (done) {
//                    System.out.println("\n[PersonalAgent] SimpleBehaviour terminated!");
//                }
//
//                return done;
//
//            }
            private void checkFund(TicketPurchase purchaseRequest) {

                //Send messages to "fa - BankFundAgent"        
                ACLMessage msg = new ACLMessage(ACLMessage.REQUEST);

                String strObj = "";
                try {
                    strObj = MyUtils.serializeObjectToString(purchaseRequest);
                } catch (Exception ex) {
                    System.out.println("\n[PersonalAgent] ObjToStr conversion error: " + ex.getMessage());
                }

                msg.setContent(strObj);
                msg.addReceiver(new AID("fa", AID.ISLOCALNAME));
                send(msg);

                System.out.println("\n[PersonalAgent] Sending Message!");
                System.out.println("[PersonalAgent] Receiver Agent                 : fa");
                System.out.println("[PersonalAgent] Message content [Base64 string]: " + strObj);

                step++;
            }

            

        });
    }

    public void resetStep() {
        step = 0;
    }
    
    public void invokePurchase(TicketPurchase purchase) {
        myPurchase = purchase;
        step = 0;
        System.out.println("[PersonalAgent] purchase invoked.");

        //Send messages to "ta - TicketAgent"        
        ACLMessage msg = new ACLMessage(ACLMessage.QUERY_IF);

        String strObj = "";
        try {
            strObj = MyUtils.serializeObjectToString(myPurchase);
        } catch (Exception ex) {
            System.out.println("\n[PersonalAgent] ObjToStr conversion error: " + ex.getMessage());
        }

        msg.setContent(strObj);
        msg.addReceiver(new AID("ta", AID.ISLOCALNAME));
        send(msg);

        System.out.println("\n[PersonalAgent] Sending Message!");
        System.out.println("[PersonalAgent] Receiver Agent                 : ta");
        System.out.println("[PersonalAgent] Message content [Base64 string]: " + strObj);

        step++;

    }

}
