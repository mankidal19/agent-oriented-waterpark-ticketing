/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package agents;

import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;
import jade.lang.acl.ACLMessage;
import utils.MyUtils;

/**
 *
 * @author NURUL AIMAN
 */
public class BankPaymentAgent extends Agent {

    private boolean serverOnline = true;

    @Override
    protected void setup() {

        MyUtils.setupControllerGUI(this);

        addBehaviour(new CyclicBehaviour() {
            @Override
            public void action() {

                ACLMessage msg = blockingReceive();

                if (msg != null) {
                    String msgContent = msg.getContent();
                    System.out.println("\n[BankPaymentAgent] Message Received");
                    System.out.println("[BankPaymentAgent] Sender Agent   : " + msg.getSender());
                    System.out.println("[BankPaymentAgent] Message content [Base64 string]: " + msgContent);
                    
                    if (serverOnline) {
                        //reply payment done
                        ACLMessage reply = new ACLMessage(ACLMessage.INFORM);

                        reply.addReceiver(msg.getSender()); //get from envelope                      

                        reply.setContent("Payment success");

                        send(reply);

                        System.out.println("[BankPaymentAgent] Payment success due to server online");

                    } else {
                        //server down
                        ACLMessage reply = new ACLMessage(ACLMessage.FAILURE);

                        reply.addReceiver(msg.getSender()); //get from envelope                      

                        reply.setPerformative(ACLMessage.FAILURE);

                        reply.setContent("Payment failed due to server offline");

                        send(reply);

                        System.out.println("[BankPaymentAgent] Payment failed due to server offline");
                    }
                }

                System.out.println("[BankPaymentAgent] CyclicBehaviour Block");
                block();
            }
        });

    }

    public void updateStatus(boolean status) {
        serverOnline = status;

    }

}
