/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import agents.*;
import gui.FakeVariablesControllerUI;
import jade.core.Agent;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;
import org.apache.commons.codec.binary.Base64;

/**
 *
 * @author NURUL AIMAN
 */
public class MyUtils {

    private static SimpleDateFormat sdf = new SimpleDateFormat("E, dd/MM/yyyy");
    private static final Base64 base64 = new Base64();
    private static FakeVariablesControllerUI myUI;
    private static WaterparkTicketAgent myTicketAgent;
    private static BankFundAgent myFundAgent;
    private static BankPaymentAgent myPaymentAgent;

    public static Date getNextDay(Date dt) {
        Calendar c = Calendar.getInstance();
        c.setTime(dt);
        c.add(Calendar.DATE, 1);
        return c.getTime();
    }

    public static Date getTodayDate() throws ParseException {
        return parseDate(new Date());
    }

    public static Date getTomorrowDate() throws ParseException {
        return parseDate(getNextDay(new Date()));
    }

    public static Date getDayAfterTomorrowDate() throws ParseException {
        return parseDate(getNextDay(getNextDay(new Date())));
    }

    public static String formatDate(Date dt) {
        return sdf.format(dt);
    }

    public static Date parseDate(Date dt) throws ParseException {
        return sdf.parse(sdf.format(dt));
    }

    //object to string
    public static String serializeObjectToString(Object object) throws IOException {
        String s = null;

        try {
            ByteArrayOutputStream arrayOutputStream = new ByteArrayOutputStream();
            GZIPOutputStream gzipOutputStream = new GZIPOutputStream(arrayOutputStream);
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(gzipOutputStream);

            objectOutputStream.writeObject(object);
            objectOutputStream.flush();
            gzipOutputStream.close();

            objectOutputStream.flush();
            objectOutputStream.close();

            s = new String(base64.encode(arrayOutputStream.toByteArray()));
            arrayOutputStream.flush();
            arrayOutputStream.close();
        } catch (Exception ex) {
            System.out.println("[MyUtils] error serializeObjectToString: " + ex);
        }

        return s;
    }

    //string to object
    public static Object deserializeObjectFromString(String objectString) throws IOException, ClassNotFoundException {
        Object obj = null;
        try {
            ByteArrayInputStream arrayInputStream = new ByteArrayInputStream(base64.decode(objectString));
            GZIPInputStream gzipInputStream = new GZIPInputStream(arrayInputStream);
            ObjectInputStream objectInputStream = new ObjectInputStream(gzipInputStream);
            obj = objectInputStream.readObject();

            objectInputStream.close();
            gzipInputStream.close();
            arrayInputStream.close();
        } catch (Exception ex) {
            System.out.println("[MyUtils] error deserializeObjectFromString: " + ex);
        }
        return obj;
    }

    //to access same controller GUI across agents
    public static void initControllerGUI() {

        if (myUI == null) {
            try {
                //myUI = new FakeVariablesControllerUI();
                myUI = new FakeVariablesControllerUI(myTicketAgent,myFundAgent,myPaymentAgent);
            } catch (ParseException ex) {
                Logger.getLogger(WaterparkTicketAgent.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public static void showControllerGUI() {
        if (!myUI.isShowing()) {
            myUI.showGui();
        }
    }

    public static FakeVariablesControllerUI getControllerGUI() {
        return myUI;
    }

    public static void setupControllerGUI(Agent agent) {

        if (agent instanceof WaterparkTicketAgent) {
            myTicketAgent = (WaterparkTicketAgent) agent;
            System.out.println("[MyUtils] setMyTicketAgent. ");
        } else if (agent instanceof BankFundAgent) {
            myFundAgent = (BankFundAgent) agent;
            System.out.println("[MyUtils] setMyFundAgent. ");
        } else if (agent instanceof BankPaymentAgent) {
            myPaymentAgent = (BankPaymentAgent) agent;
            System.out.println("[MyUtils] setMyPaymentAgent. ");
        }

        if (myTicketAgent != null && myFundAgent != null && myPaymentAgent != null) {
            initControllerGUI();
            showControllerGUI();
        }

//        if (agent instanceof WaterparkTicketAgent) {
//            myUI.setMyTicketAgent((WaterparkTicketAgent) agent);
//            System.out.println("[MyUtils] setMyTicketAgent. ");
//        } else if (agent instanceof BankFundAgent) {
//            myUI.setMyFundAgent((BankFundAgent) agent);
//            System.out.println("[MyUtils] setMyFundAgent. ");
//        } else if (agent instanceof BankPaymentAgent) {
//            myUI.setMyPaymentAgent((BankPaymentAgent) agent);
//            System.out.println("[MyUtils] setMyPaymentAgent. ");
//        }
    }

    public static void updateFundBalanceUI(double fund) {
        myUI.setFundValue(fund);
    }
    
    public static void resetStepAllAgent(){
        myUI.getMyFundAgent().resetStep();
        myUI.getMyTicketAgent().resetStep();
        
    }
    
     public static void updateTicketAvailableUI(Date date, int adultQty, int childQty, int seniorQty){
         myUI.updateTicketAvailable(date, adultQty, childQty, seniorQty);
     }
}
