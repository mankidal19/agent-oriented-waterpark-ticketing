/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.io.Serializable;
import java.util.*;

/**
 *
 * @author NURUL AIMAN
 */
public class TicketPurchase implements Serializable {
    
    private Date date;
    private Hashtable purchase;
    private String txnCode;
    private double totalAmount;

    public TicketPurchase(Date date) {
        this.date = date;
        purchase = new Hashtable();
        
    }

    public TicketPurchase(Date date, Hashtable purchase) {
        this.date = date;
        this.purchase = purchase;
    }

    public TicketPurchase() {
        
        
    }
    
    

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Hashtable getPurchase() {
        return purchase;
    }

    public void setPurchase(Hashtable purchase) {
        this.purchase = purchase;
    }

    public String getTxnCode() {
        return txnCode;
    }

    public void setTxnCode(String txnCode) {
        this.txnCode = txnCode;
    }

    public double getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(double totalAmount) {
        this.totalAmount = totalAmount;
    }
    
    
    
    
}
