/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import agents.*;
import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author NURUL AIMAN
 */
public class TicketAvailable implements Serializable {
    private Ticket ticket;
    private Date date;
    private int quantity;

    public TicketAvailable(Ticket ticket, Date date, int quantity) {
        this.ticket = ticket;
        this.date = date;
        this.quantity = quantity;
    }

    public Ticket getTicket() {
        return ticket;
    }

    public void setTicket(Ticket ticket) {
        this.ticket = ticket;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
    
    
}
