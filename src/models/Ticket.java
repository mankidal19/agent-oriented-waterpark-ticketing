/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.io.Serializable;

/**
 *
 * @author NURUL AIMAN
 */
public class Ticket implements Serializable {
    
    private Type type;
    private double price;

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }


    public Ticket(Type type, double price) {
        this.type = type;
        this.price = price;
    }
    
    
    
    public enum Type{
    ADULT,
    CHILD,
    SENIOR
}
}


